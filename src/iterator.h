#pragma once

#include <queue>
#include "tree.h"

//abstract iterator
template<typename T>
class Iterator {
public:
    virtual bool has_next() = 0;
    virtual void next() = 0;
    virtual T get_item() = 0;
};

//Concrete iterator
class TreeIterator : public Iterator<int> {
public:
    TreeIterator(BaseTree *);
    virtual bool has_next();
    virtual void next();
    virtual int get_item();
private:

    BaseTree * current;
    std::queue<BaseTree*> pending_items; //queue of pending elements (see https://en.wikipedia.org/wiki/Breadth-first_search)
    void enqueue_child();
};

class IterableTree : public BaseTree {
public:
    TreeIterator first();
    IterableTree(int);
};