#include "tree.h"


BaseTree * BaseTree::go_left() {
    return left;
}

BaseTree * BaseTree::go_right() {
    return right;
}

BaseTree::BaseTree(int item){
    left = nullptr;
    right = nullptr;
    this->item = item;
}


int BaseTree::get_item() {
    return item;
}



void BaseTree::add_elem(int item) {
    direction d ;
    BaseTree *current = this;
    BaseTree * next = this;

    //find leaf
    while (next != nullptr) {
        int ci = current->item;
        d = item < ci ? LEFT : RIGHT;
        next = current->go_to(d);
        if (next != nullptr) {
            current = next;
        }
    }
    //add new child for found leaf
    current->get_ref(d) = new BaseTree(item);
}

BaseTree *BaseTree::go_to(direction direction1) {
    return direction1 == LEFT ? left : right;
}

BaseTree *& BaseTree::get_ref(direction direction1) {
    return direction1 == LEFT ? left : right;
}


BaseTree::~BaseTree() {
    if (left != nullptr) {
        delete left;
    }
    if (right != nullptr) {
        delete right;
    }


}
