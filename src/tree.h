#pragma once

enum direction {
    LEFT, RIGHT
};

//abstract aggregate
class BaseTree {
protected:
    BaseTree *left;
    BaseTree *right;
    int item;

    BaseTree *&get_ref(direction);
    BaseTree() = delete;
    BaseTree(BaseTree&) = delete;

public:
    ~BaseTree();
    BaseTree(int item);
    void add_elem(int item);
    int get_item();
    BaseTree *go_left();
    BaseTree *go_right();
    BaseTree *go_to(direction);
};






