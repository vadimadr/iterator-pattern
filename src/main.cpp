#include <algorithm>
#include <iostream>
#include "tree.h"
#include "iterator.h"


class Client {
public:
    void exec() {
        IterableTree t(5);

        std::vector<int> v = {10, 3, 2, 15, 7};
        std::for_each(v.begin(), v.end(), [&t](int &i) {
            t.add_elem(i);
        });

        TreeIterator it = t.first();
        while (it.has_next()) {
            std::cout << it.get_item() << std::endl;
            it.next();
        }

    }
};

int main(int argc, char *argv[]) {
    Client c;
    c.exec();
    return 0;
}