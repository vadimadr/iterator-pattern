#include <iostream>
#include <vector>
#include "test.h"
#include "tree.h"
#include "iterator.h"

void iter_test();


void run_all_test() {
    tree_create_test();
    iter_test();
}


void tree_create_test() {
    //create some tree
    BaseTree *top = new BaseTree(5);
    top->add_elem(10);
    top->add_elem(3);
    top->add_elem(2);
    top->add_elem(15);
    std::vector<int> v;
    v.push_back(top->get_item()); //5
    v.push_back(top->go_right()->get_item()); //10
    v.push_back(top->go_left()->get_item()); //3
    v.push_back(top->go_right()->go_right()->get_item()); //15
    v.push_back(top->go_left()->go_left()->get_item()); //2
    std::vector<int> v2 {5, 10, 3, 15, 2};
    std::cout << (v == v2) << std::endl;
}


void iter_test() {
    //create some tree
    IterableTree *top = new IterableTree(5);
    top->add_elem(10);
    top->add_elem(3);
    top->add_elem(2);
    top->add_elem(15);
    top->add_elem(7);
    TreeIterator it = top->first();
    //trying to output it
    //should be 5 3 10 2 7 15
    //see https://en.wikipedia.org/wiki/Breadth-first_search
    while (it.has_next()) {
        std::cout << it.get_item() << " " ;
        it.next();
    }
    std::cout << std::endl;
}