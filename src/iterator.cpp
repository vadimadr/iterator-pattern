#include "iterator.h"

bool TreeIterator::has_next() {
    return !pending_items.empty() || current != nullptr;
}

void TreeIterator::next() {
    //enqueue element if any
    if (!pending_items.empty()) {
        current = pending_items.front();
        pending_items.pop();
    }
    else current = nullptr;
    enqueue_child();
}

int TreeIterator::get_item() {
    return current->get_item();
}

TreeIterator::TreeIterator(BaseTree *tree) : current(tree) {
   enqueue_child();
}

void TreeIterator::enqueue_child() {
    //push children to queue
    if (current == nullptr) {
        return;
    }
    BaseTree *child = current->go_left();
    if (child != nullptr) pending_items.push(child);
    child = current->go_right();
    if (child != nullptr) pending_items.push(child);
}


TreeIterator IterableTree::first() {
    return TreeIterator(this);
}

IterableTree::IterableTree(int i)  : BaseTree(i) {
}
